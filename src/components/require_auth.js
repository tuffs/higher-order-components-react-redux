import React, { Component } from 'react';
import { connect } from 'react-redux';

export default function(ComposedComponent) {
  class Authentication extends Component {
    static contextTypes = {
      router: React.PropTypes.object
    }

    componentWillMount() {
      if(!this.props.authenticated) this.context.router.push('/');
    }

    componentWillUpdate(nextProps) {
      if(!nextProps.authenticated) this.context.router.push('/');
    }

    render() {
      return <ComposedComponent {...this.props} />
    }
  }

  function mapStateToProps(state) {
    return { authenticated: state.authenticated };
  }

  return connect(mapStateToProps)(Authentication);
}

// In some other location (not this file) - use this HoC
// Example Use:

// ``` import Authentication // HoC
// ``` import Resources // Component to Wrap in Authentication
// ``` const ComposedComponent = Authentication(Resources);

// In render:

// ``` <ComposedComponent />
